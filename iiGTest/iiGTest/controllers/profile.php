<?php
	session_start();
	if ($_SESSION["currentUserID"] == null) {
		echo 	"<script type=\"text/javascript\">
					alert('Cannot go to this page.'); 
					window.location.href = '../login.html';
				</script>";
	}
	
	include "../libs/phptemplate/template.php";
	$template = new Template();
	
	$menu = file_get_contents("../views/sidebar-menu.html");
	$template->assign("menu", $menu);

	$page = file_get_contents("../views/profile.html");
	$page .= file_get_contents("../views/modal-user-info.html");
	$template->assign("content", $page);

	$template->assign("pagename", "Your Profile");

	$template->parse("../templates/main-template.html");
?>