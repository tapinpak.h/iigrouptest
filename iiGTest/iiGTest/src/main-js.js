function passwordSetUp(item) {
	if (item.find('.password').attr('type') === "password") {
		item.find('.password').attr('type', 'text');
		item.find('.password').closest('div').find('i').removeClass('bi-eye-slash-fill');
		item.find('.password').closest('div').find('i').addClass('bi-eye-fill');
	} else {
		resetPasswordField(item);
	}
}
function resetPasswordField(item) {
	item.find('.password').attr('type', 'password');
	item.find('.password').closest('div').find('i').removeClass('bi-eye-fill');
	item.find('.password').closest('div').find('i').addClass('bi-eye-slash-fill');
}

function hashPassword(text) {
	var encrypted = CryptoJS.MD5(CryptoJS.MD5(CryptoJS.MD5(text)));
    return encrypted.toString(CryptoJS.enc.MD5);
}

function clearForm(form) {
	form.find('input').val('');
	form.find('textarea').val('');
	form.find('select').val('');
}

function setdataFilter(data) {
	var result = JSON.parse(data);

	if (result.ErrorMessage != null) {
		$('#ErrorMessage').val(result.ErrorMessage);
		return false;
	} else {
		return true;
	}
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}

$(document).ready(function () {

	$.validator.setDefaults({
		ignore: ':hidden, [readonly=readonly]'
	});
	$.validator.addMethod('username', function (value, element) {
		return this.optional(element) || /^[a-zA-Z0-9_]{6,12}$/.test(value);
	}, 'Please fill form with alphabets or numbers.');
	$.validator.addMethod('password', function (value, element) {
		return this.optional(element) || /^[A-Za-z0-9@$!%*#?&;\-_]{6,20}$/g.test(value);
	}, 'Please fill form with alphabets, numbers or special charector as (@$!%*#?&;\-_)');
	$.validator.addMethod('cannotContinue', function (value, element) {
		var correct = true;
		for (var i = 0; i < value.length; i++) {
			if (i === 0) {
				continue;
			} else {
				let diff = value[i].charCodeAt() - value[i - 1].charCodeAt();
				if (diff === 1 || diff === -1) {
					correct = false;
				}
			}
		}
		return correct;
	}, 'Please user another password.');

	$('[data-target="#modalUserInfo"]').on('click', function() {
		$('#modalUserInfo').modal('show');
	});
	$('button[data-dismiss="modal"]').on('click', function() {
		$(this).closest('.modal').modal('hide');
	});
});