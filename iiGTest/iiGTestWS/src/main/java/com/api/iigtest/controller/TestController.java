package com.api.iigtest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/v1/test")
public class TestController {
	
	@GetMapping()
	public String getUser(@PathVariable Integer id) {
		return "Hi there";
	}

}
