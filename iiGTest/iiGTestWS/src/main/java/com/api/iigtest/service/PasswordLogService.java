package com.api.iigtest.service;

import com.api.iigtest.model.PasswordLog;

public interface PasswordLogService {
	
	PasswordLog insertNewPasswordLog(PasswordLog passwordLog);
	boolean checkCanUseThisPassword(Integer userID, String password);

}
