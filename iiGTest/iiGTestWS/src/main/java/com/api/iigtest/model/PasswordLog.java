package com.api.iigtest.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="password_log")
public class PasswordLog {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "user_id", nullable = false)
	private Integer userID;
	
	@Column(name = "password", nullable = false)
	private String password;
	
	@Column(name = "created_date")
	private Date createdDate;

	public PasswordLog() {}
	
	public PasswordLog(Integer userID, String password) {
		super();
		this.userID = userID;
		this.password = password;
		this.createdDate = new Date();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		if (createdDate == null) {
			this.createdDate = new Date();
		}
		this.createdDate = createdDate;
	}
	
	
}
