package com.api.iigtest.service.imp;

import java.util.List;

import org.springframework.stereotype.Service;

import com.api.iigtest.model.User;
import com.api.iigtest.repository.UserRepository;
import com.api.iigtest.service.UserService;

@Service
public class UserServiceImp implements UserService {
	
	private UserRepository userRepository;
	
	public UserServiceImp(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}

	public User createNewUser(User user) {
		return userRepository.save(user);
	}

	public boolean checkCanUseThisUsername(String username) {
		List<User> users = userRepository.findByUsername(username);
		if (users.size() > 0) {
			return false;
		} else {
			return true;
		}
	}

	public User getUser(Integer userID) {
		return userRepository.findFirstById(userID);
	}

	public User getUser(String username, String password) {
		return userRepository.findFirstByUsernameAndPassword(username, password);
	}

	public User updateUserInfo(Integer userID, User user) {
		User oldData = userRepository.findFirstById(userID);
		
		oldData.setFirstname(user.getFirstname());
		oldData.setLastname(user.getLastname());
		if (user.getPassword() != null && !user.getPassword().equals(""))
			oldData.setPassword(user.getPassword());

		userRepository.save(oldData);
		return oldData;
	}
	
	public User updateUserProfileImg(Integer userID, byte[] file) {
		User oldData = userRepository.findFirstById(userID);
		
		oldData.setProfileImg(file);
			
		userRepository.save(oldData);
		return oldData;
	}

}
