package com.api.iigtest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.iigtest.model.PasswordLog;

public interface PasswordLogRepository extends JpaRepository<PasswordLog, Integer> {

	List<PasswordLog> findTop5ByUserIDOrderByCreatedDateDesc(Integer userID);

}
