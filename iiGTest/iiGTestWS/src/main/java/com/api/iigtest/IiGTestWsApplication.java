package com.api.iigtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IiGTestWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(IiGTestWsApplication.class, args);
	}

}
