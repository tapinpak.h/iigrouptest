package com.api.iigtest.service;

import com.api.iigtest.model.User;

public interface UserService {
	
	User getUser(Integer userID);
	User getUser(String username, String password);
	User createNewUser(User user);
	User updateUserInfo(Integer userID, User user);
	User updateUserProfileImg(Integer userID, byte[] file);
	boolean checkCanUseThisUsername(String username);
	
}
