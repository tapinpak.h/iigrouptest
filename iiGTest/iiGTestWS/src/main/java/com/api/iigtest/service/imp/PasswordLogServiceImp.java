package com.api.iigtest.service.imp;

import java.util.List;

import org.springframework.stereotype.Service;

import com.api.iigtest.model.PasswordLog;
import com.api.iigtest.repository.PasswordLogRepository;
import com.api.iigtest.service.PasswordLogService;

@Service
public class PasswordLogServiceImp implements PasswordLogService {
	
	private PasswordLogRepository passwordLogRepository;
	
	public PasswordLogServiceImp(PasswordLogRepository passwordLogRepository) {
		super();
		this.passwordLogRepository = passwordLogRepository;
	}

	public PasswordLog insertNewPasswordLog(PasswordLog password) {
		return passwordLogRepository.save(password);
	}

	public boolean checkCanUseThisPassword(Integer userID, String password) {
		List<PasswordLog> passwordLogs = passwordLogRepository.findTop5ByUserIDOrderByCreatedDateDesc(userID);
		for(PasswordLog log : passwordLogs) {
			if (log.getPassword().equals(password)) {
				return false;
			}
		}
		return true;
	}

}
