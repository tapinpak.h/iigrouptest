package com.api.iigtest.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.api.iigtest.model.PasswordLog;
import com.api.iigtest.model.User;
import com.api.iigtest.service.PasswordLogService;
import com.api.iigtest.service.UserService;

@CrossOrigin(origins = "http://localhost:9393/", allowedHeaders = "*")
@RestController
@RequestMapping("/v1/user")
public class UserController {

	private UserService userService;
	private PasswordLogService passwordLogService;

	public UserController(UserService userService, PasswordLogService passwordLogService) {
		super();
		this.userService = userService;
		this.passwordLogService = passwordLogService;
	}

	@GetMapping("/{id}")
	public User getUser(@PathVariable Integer id) {
		return userService.getUser(id);
	}
	
	@GetMapping("/{username}/{password}")
	public User getUser(@PathVariable("username") String username, @PathVariable("password") String password) {
		return userService.getUser(username, password);
	}
	
	@GetMapping("/")
	public User checkLogin(@RequestParam("username") String username, @RequestParam("password") String password) {
		return userService.getUser(username, password);
	}

	@GetMapping("/check/username/{username}")
	public boolean isUserNameUsed(@PathVariable String username) {
		return userService.checkCanUseThisUsername(username);
	}
	
	@GetMapping("/check/username/")
	public boolean isUserNameUsedAJAX(@RequestParam("username") String username) {
		return userService.checkCanUseThisUsername(username);
	}

	@GetMapping("/check/password/{userID}/{password}")
	public boolean canUseThisPass(@PathVariable("userID") Integer userID, @PathVariable("password") String password) {
		return passwordLogService.checkCanUseThisPassword(userID, password);
	}
	
	@GetMapping("/check/password")
	public boolean canUseThisPassAJAX(@RequestParam("userID") Integer userID, @RequestParam("password") String password) {
		return passwordLogService.checkCanUseThisPassword(userID, password);
	}

	@RequestMapping(value = "", method = RequestMethod.POST, consumes = "application/json")
	@CrossOrigin(origins = "*")
	public ResponseEntity<User> createUser(@RequestBody User user) {
		User createdInfo = userService.createNewUser(user);
		PasswordLog passwordLog = new PasswordLog(createdInfo.getId(), createdInfo.getPassword()); 
		passwordLogService.insertNewPasswordLog(passwordLog);
		
		return new ResponseEntity<User>(createdInfo, HttpStatus.CREATED);
	}

	@PatchMapping("/{userID}")
	@CrossOrigin(origins = "*")
	public ResponseEntity<User> updateUserInfo(@PathVariable("userID") Integer userID, @RequestBody User user) {
		if (user.getPassword() != null && !user.getPassword().equals("")) {
			PasswordLog passwordLog = new PasswordLog(userID, user.getPassword()); 
			passwordLogService.insertNewPasswordLog(passwordLog);
		}
		return new ResponseEntity<User>(userService.updateUserInfo(userID, user), HttpStatus.OK);
	}
	
	@GetMapping("/{userID}/profileImg")
	@ResponseBody
    public void downloadFile(@PathVariable("userID") Integer userID, HttpServletResponse response) throws IOException {
		User user = userService.getUser(userID);
		response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
		response.getOutputStream().write(user.getProfileImg());
		response.getOutputStream().close();
	}
	
	
	@PatchMapping("/{userID}/profileImg")
	@CrossOrigin(origins = "*")
    public ResponseEntity<User> uploadFile(@PathVariable("userID") Integer userID, @RequestParam("file") MultipartFile file) throws Exception {
        return new ResponseEntity<User>(userService.updateUserProfileImg(userID, file.getBytes()), HttpStatus.OK);
    }
}
