SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS iig_test;

USE iig_test;

CREATE TABLE IF NOT EXISTS user (
  id int NOT NULL AUTO_INCREMENT,
  username varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  password varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  firstname varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  lastname varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  profile_img longblob NULL,
  created_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_date datetime DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY username (username)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS password_log (
  id int NOT NULL AUTO_INCREMENT,
  user_id int NOT NULL,
  password varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  created_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES user(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

COMMIT;